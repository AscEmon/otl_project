
import 'package:flutter/material.dart';
import 'package:dio/dio.dart' as d;
import 'package:otl_project/data_provider/api_client.dart';
import 'package:otl_project/data_provider/pref_helper.dart';
import 'package:otl_project/mvc/login_module/model/login_response.dart';
import 'package:otl_project/utils/enum.dart';
import 'package:otl_project/utils/navigation_service.dart';
import 'package:otl_project/utils/styles/styles.dart';
import 'package:otl_project/utils/view_util.dart';
import '../../../constant/app_url.dart';

class LoginController extends ChangeNotifier {
  final loginKey = GlobalKey<FormState>();
  TextEditingController userId = TextEditingController();
  TextEditingController password = TextEditingController();
  ApiClient apiClient = ApiClient();

  void login(BuildContext context) async {
    if (loginKey.currentState!.validate()) {
      final map = Map<String, dynamic>();
      map['emp_id'] = userId.text.toString();
      map['password'] = password.text.toString();

      try {
        ViewUtil.showAlertDialog(
          content: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              CircularProgressIndicator.adaptive(),
              SizedBox(width: 10.w),
              Text("Loading...")
            ],
          ),
        );
        await apiClient.request(
            url: AppUrl.LOGIN_URL,
            method: Method.POST,
            params: map,
            onSuccessFunction: (d.Response response) {
              if (response.data != null) {
                var data = LoginResponse.fromJson(response.data);
                if (data.code == 200) {
                  if (data.data != null) {
                    // PrefHelper.setString(TOKEN, data.data?.token ?? "");
                    // PrefHelper.setString(
                    //     EMPLOYEE_ID, data.data?.user?.empId.toString() ?? "");
                    // ProjectListScreen().pushAndRemoveUntil(context);
                  }
                }
              }
            });
      } on Exception catch (e) {
        Navigation.pop(context);
      } finally {
        notifyListeners();
      }
    }
  }
}
