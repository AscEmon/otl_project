
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:otl_project/mvc/login_module/views/login_screen.dart';
import 'package:otl_project/utils/extention.dart';
import 'package:otl_project/utils/styles/styles.dart';

class LoginButton extends StatelessWidget {
  const LoginButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final loginController = ref.read(loginProvider);
      return Container(
        width: double.infinity,
        height: 52.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8), color: Color.fromARGB(255, 98, 14, 134)),
        child: TextButton(
          style: TextButton.styleFrom(
            primary: Colors.white,
          ),
          onPressed: () {
            FocusScope.of(context).unfocus();
            loginController.login(context);
          },
          child: Text(
            context.loc.login_btn,
            style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
          ),
        ),
      );
    });
  }
}
