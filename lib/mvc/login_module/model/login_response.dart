class LoginResponse {
  LoginResponse({
    this.code,
    this.messages,
    this.data,
  });

  int? code;
  List<String>? messages;
  LoginData? data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        code: json["code"] == null ? null : json["code"],
        messages: json["messages"] == null
            ? null
            : List<String>.from(json["messages"].map((x) => x)),
        data: json["data"] == null ? null : LoginData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
        "messages": messages == null
            ? null
            : List<dynamic>.from(messages!.map((x) => x)),
        "data": data == null ? null : data!.toJson(),
      };
}

class LoginData {
  LoginData({
    this.user,
    this.token,
  });

  User? user;
  String? token;

  factory LoginData.fromJson(Map<String, dynamic> json) => LoginData(
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        token: json["token"] == null ? null : json["token"],
      );

  Map<String, dynamic> toJson() => {
        "user": user == null ? null : user!.toJson(),
        "token": token == null ? null : token,
      };
}

class User {
  User({
    this.id,
    this.empId,
    this.name,
    this.email,
    this.contactNo,
    this.departmentId,
    this.designationId,
    this.departmentHead,
    this.reportingTo,
    this.employeeCategory,
    this.categoryExpireDate,
    this.checkAttendance,
    this.punchEligibility,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.designation,
    this.department,
    this.profilePic,
  });

  int? id;
  int? empId;
  String? name;
  String? email;
  String? contactNo;
  int? departmentId;
  int? designationId;
  int? departmentHead;
  int? reportingTo;
  String? employeeCategory;
  dynamic categoryExpireDate;
  String? checkAttendance;
  int? punchEligibility;
  String? status;
  dynamic createdAt;
  DateTime? updatedAt;
  String? designation;
  String? department;
  String? profilePic;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        empId: json["emp_id"] == null ? null : json["emp_id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        contactNo: json["contact_no"] == null ? null : json["contact_no"],
        departmentId:
            json["department_id"] == null ? null : json["department_id"],
        designationId:
            json["designation_id"] == null ? null : json["designation_id"],
        departmentHead:
            json["department_head"] == null ? null : json["department_head"],
        reportingTo: json["reporting_to"] == null ? null : json["reporting_to"],
        employeeCategory: json["employee_category"] == null
            ? null
            : json["employee_category"],
        categoryExpireDate: json["category_expire_date"],
        checkAttendance:
            json["check_attendance"] == null ? null : json["check_attendance"],
        punchEligibility: json["punch_eligibility"] == null
            ? null
            : json["punch_eligibility"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        designation: json["designation"] == null ? null : json["designation"],
        department: json["department"] == null ? null : json["department"],
        profilePic: json["profile_pic"] == null ? null : json["profile_pic"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "emp_id": empId == null ? null : empId,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "contact_no": contactNo == null ? null : contactNo,
        "department_id": departmentId == null ? null : departmentId,
        "designation_id": designationId == null ? null : designationId,
        "department_head": departmentHead == null ? null : departmentHead,
        "reporting_to": reportingTo == null ? null : reportingTo,
        "employee_category": employeeCategory == null ? null : employeeCategory,
        "category_expire_date": categoryExpireDate,
        "check_attendance": checkAttendance == null ? null : checkAttendance,
        "punch_eligibility": punchEligibility == null ? null : punchEligibility,
        "status": status == null ? null : status,
        "created_at": createdAt,
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "designation": designation == null ? null : designation,
        "department": department == null ? null : department,
        "profile_pic": profilePic == null ? null : profilePic,
      };
}
