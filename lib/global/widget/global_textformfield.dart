import 'package:flutter/material.dart';
import 'package:otl_project/utils/styles/styles.dart';


class GlobalTextFormField extends StatelessWidget {
  bool obscureText = false;
  TextInputType? textInputType = TextInputType.text;
  TextEditingController? controller;
  String? Function(String?)? validator;
  Widget? suffixIcon;
  Widget? prefixIcon;
  int? maxlength;
  AutovalidateMode? autovalidateMode;
  bool readOnly = false;
  Color? fillColor;
  String? hintText;
  TextStyle? hintStyle;
  TextStyle? style;
  int? line;
  String? initialValue;
  TextInputAction? textInputAction;

  GlobalTextFormField({
    Key? key,
    this.obscureText = false,
    this.textInputType,
    this.controller,
    this.validator,
    this.fillColor,
    this.suffixIcon,
    this.prefixIcon,
    this.maxlength,
    this.initialValue,
    this.autovalidateMode,
    this.readOnly = false,
    this.hintText,
    this.hintStyle,
    this.line = 1,
    this.style,
    this.textInputAction = TextInputAction.done,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 52.h,
      child: TextFormField(
        initialValue: initialValue,
        maxLines: line,
        style: style == null
            ? KTextStyle.customTextStyle()
            : style,
        autovalidateMode: autovalidateMode,
        obscureText: obscureText,
        obscuringCharacter: '*',
        controller: controller,
        textInputAction: textInputAction,
        cursorColor: Colors.black,
        keyboardType: textInputType,
        maxLength: maxlength,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        decoration: InputDecoration(
          prefixIcon: prefixIcon,
          hintText: hintText,
          filled: true,
          counterText: "",
          fillColor: Colors.white,
          suffixIcon: suffixIcon,
          hintStyle: hintStyle == null ? KTextStyle.customTextStyle(color: Colors.grey, fontSize: 14) : hintStyle,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
          ),
          // focusedBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(12),
          //   borderSide: BorderSide(color: Colors.black, width: 1.w),
          // ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1.w),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 1.w),
            borderRadius: BorderRadius.circular(12),
          ),
          // enabledBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(12),
          //   borderSide: BorderSide.none,
          // ),
        ),
        validator: validator,
        readOnly: readOnly,
      ),
    );
  }
}
