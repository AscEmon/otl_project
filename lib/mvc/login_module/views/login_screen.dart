import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:otl_project/mvc/login_module/controller/login_controller.dart';
import 'package:otl_project/mvc/login_module/views/components/form_part.dart';
import 'package:otl_project/mvc/login_module/views/components/login_button.dart';
import 'package:otl_project/utils/extention.dart';
import 'package:otl_project/utils/styles/styles.dart';

final loginProvider = ChangeNotifierProvider((ref) => LoginController());

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: context.height,
          width: context.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromARGB(255, 38, 23, 53),
                Color.fromARGB(255, 8, 3, 19),
              ],
            ),
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 100.h,
                ),
                Container(
                  padding: EdgeInsets.all(15.w),
                  margin: EdgeInsets.all(15.w),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.w),
                    color: Color.fromARGB(255, 255, 255, 255).withOpacity(.06),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 20.h,
                      ),
                      Text(context.loc.login_btn,
                          style: TextStyle(
                              letterSpacing: 1,
                              fontSize: 24.sp,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      SizedBox(
                        height: 20.h,
                      ),
                      //Textformfield in form
                      Padding(
                        padding:
                            EdgeInsets.only(left: 8.0.w, right: 8.w, top: 8.h),
                        child: FormPart(),
                      ),

                      Padding(
                        padding: EdgeInsets.all(8.0.w),
                        child: LoginButton(),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0.w),
                        child: Text(
                          context.loc.for_account_creation_and_password,
                          textAlign: TextAlign.center,
                          style: KTextStyle.customTextStyle(
                            color: Colors.white.withOpacity(.5),
                            fontSize: 12.sp,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.h,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
