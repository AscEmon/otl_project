
import 'package:flutter/material.dart';
import 'package:otl_project/utils/extention.dart';

class LoginHeaderImage extends StatelessWidget {
  const LoginHeaderImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.width,
      height: context.height * .25,
      child: Opacity(
        opacity: 0.5,
        child: Image.asset(
          'assets/images/login_bg.png',
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
