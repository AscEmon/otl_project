
import 'package:flutter/material.dart';
import 'package:otl_project/global/widget/global_textformfield.dart';
import 'package:otl_project/mvc/login_module/views/login_screen.dart';
import 'package:otl_project/utils/extention.dart';
import 'package:otl_project/utils/styles/styles.dart';

class FormPart extends StatelessWidget {
  const FormPart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: context.read(loginProvider).loginKey,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            child: Text(
              context.loc.user_name_placeholder,
              textAlign: TextAlign.left,
              style: KTextStyle.customTextStyle(
                color: Colors.white,
                fontSize: 12.sp,
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.only(top: 10.h, bottom: 16.h),
              child: GlobalTextFormField(
                controller: context.read(loginProvider).userId,
                hintText: context.loc.user_name_placeholder,
                textInputType: TextInputType.number,
                validator: (value) {
                  if (value!.trim().isEmpty)
                    return context.loc.user_id_required;
                  else
                    return null;
                },
              ),
            ),
          ),
          Container(
            width: double.infinity,
            child: Text(
              context.loc.password_placeholder,
              textAlign: TextAlign.left,
              style: KTextStyle.customTextStyle(
                color: Colors.white,
                fontSize: 12.sp,
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.only(top: 10.h, bottom: 16.h),
              child: GlobalTextFormField(
                controller: context.read(loginProvider).password,
                obscureText: true,
                hintText: context.loc.password_placeholder,
                validator: (value) {
                  if (value!.trim().isEmpty)
                    return context.loc.password_required;
                  else
                    return null;
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
